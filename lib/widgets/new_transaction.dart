import 'package:flutter/material.dart';

class NewTransaction extends StatefulWidget {
  final Function addTx;

  NewTransaction(this.addTx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final titleController = TextEditingController();

  final amountController = TextEditingController();

  void submitData() {
    final enteredTitle = titleController.text;
    final enteredAmount = double.parse(amountController
        .text); //double parse buat ngubah jadi double dari text.

    if (enteredTitle.isEmpty || enteredAmount <= 0) {
      return; //return jadi nge berentiin codenya ga dilanjutin.
    }

    

    widget.addTx(enteredTitle, enteredAmount);

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      //textfield card
      elevation: 5, //shadow dari card ini
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment:
              CrossAxisAlignment.end, // ini buat posisi buttonnya
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: "Title "),
              controller: titleController,
              onSubmitted: (_) => submitData(),
              // onChanged: (value) {
              //   titleInput = value;
              // },
            ),
            TextField(
              decoration: InputDecoration(labelText: "Ammount"),
              controller: amountController,
              keyboardType: TextInputType.number,
              onSubmitted: (_) => submitData(),
              // onChanged:s (value) {
              //   amountInput = value;
              // },
            ),
            FlatButton(
              child: Text("add Transaction"),
              textColor: Colors.blueAccent,
              onPressed: submitData,
            )
          ],
        ),
      ),
    );
  }
}
